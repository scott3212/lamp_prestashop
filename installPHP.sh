#!/bin/bash

sudo apt-get -y install php libapache2-mod-php php-mcrypt php-mysql php-zip php-curl php-xml php-gd php-intl
cd ~
echo '<?php' > info.php
echo 'phpinfo();' >> info.php
echo '?>' >> info.php
sudo mv info.php /var/www/html/
sudo systemctl restart apache2

echo "Delete file /var/www/html/info.php after testing php works"
