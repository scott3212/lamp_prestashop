#!/bin/bash

wget https://download.prestashop.com/download/releases/prestashop_1.7.1.2.zip
sudo unzip prestashop_1.7.1.2.zip -d /var/www/html/
sudo chown -R www-data:www-data /var/www/
