This is a repository contains installation scripts of Prestashop under Ubuntu Server 16.04.

Pre-Requirement:
# Run a sudo command, to prevent script asking password.
# enableSSHLogin.sh
  #Copy the commands in it and run in new Ubuntu, and the following command can run from putty
# Install Git:
  sudo apt-get -y install git
# Install Vim:
  sudo apt-get -y install vim
# Add public key to projct
  #bitbucket.org:scott3212
# Clone this repository:
  ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
  git clone git@bitbucket.org:scott3212/lamp_prestashop.git